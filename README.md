# CI-CD-APPS

Listing CI-CD Usefull apps to be integrated to your repositories

I will try to make an order. Not actually by relevance, but an suggestion of integration order, that is not required to be followed.

1. Code Style
1. Security
1. Tests
1. Linting
1. Deployment

Interesting to have:

1. Git Flow
1. E2E Test

## Private repository using free accounts - Suggestion

- Security: snyk

## CI-CD Workflow

Usually also does linting, testing and deployment.

- [Bitbucket Pipelines](https://bitbucket.org/) - Pipelines to automate deployment **[free account]** [linting, test, deployment]
- [Buddy](https://app.buddy.works) - linting, test, deployment [free account]
- [Circle CI](https://circleci.com) - Automate Software Testing and Delivery
- [Codacy](https://codacy.com) - Automate Software Testing and Delivery. SOme limitations on free tier. [lintint, test, coverage, pr-review]
- [Code Climate](https://codeclimate.com) - Automated deployment **free for public repos** **free for up to 4 people**
- [Codefresh](https://codefresh.io) - CI system much like TravisCI or CircleCI, but aimed towards container-heavy kubernetes-forward workloads.
- Jenkins [linting, test, deployment]
- [GitLab](https://gitlab.com) - CI/CD automated platform
- [Travis CI](https://travis-ci.com) - Automate Software Testing and Delivery

**To be tested**
- [Drone CI](https://drone.io) - Automate Software Testing and Delivery
- [HoundCI](https://houndci.com) **[free for public repos]**

## Code Style

- [Code Climate](https://codeclimate.com)
- [HoundCI](https://houndci.com) **[free for public repos]**

## Security

- [Snyk](https://snyk.io) - Finding and fixing dependencies vulnerabilities **[free account]**
- [LGTM](https://lgtm.com) -  Finding and fixing dependencies vulnerabilities **[free account]**

**To be tested**
- [Code Notary](https://www.codenotary.io) - Signing and securing docker images
- [Dependabot](https://dependabot.com) - pull requests to keep your dependencies secure and up-to-date.

## Deployment

Those can deploy your application into their network

**To be tested**
- [Netlify](https://netlify.com) - An all-in-one workflow that combines global deployment, continuous integration, and automatic HTTPS. And that’s just the beginning. With preview deployment. **[free account]**

## Performance

- [Gimbal](https://github.com/ModusCreateOrg/gimbal) - Web Performance Auditing tooling

## Git flow


- [PRlint](https://github.com/apps/prling) - You want your pull requests to have a consistent convention for titles, descriptions, branch names, labels, milestones, and more. **[free account]**

## E2E Test

- [Buddy](https://app.buddy.works) - Trigger browser, E2E, and visual tests on every deployment with the official Ghost Inspector integration.

